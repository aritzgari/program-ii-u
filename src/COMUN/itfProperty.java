
package COMUN;

/**
 * Esta es la inerfaz para comunicaciones entre las capas LP y LN.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public interface itfProperty {

	/**
	 * Esta es el metodo para transferir información de un cantante entre LP y LN.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E3
	 */
	Object getProperty(String columna);

}