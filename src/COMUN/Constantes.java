package COMUN;

/**
 * Esta es la clase que contiene las queris de inserci�n y consulta.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */
public class Constantes {
	/**
	 * Constantes para querys de Librerias.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public static final String queryInsertLibreria = "insert into Libreria_Multimedia (Nombre, Descripcion) values (?, ?)";
	public static final String queryConsultaLibreria = "SELECT * FROM lando.libreria_multimedia;";

	/**
	 * Constantes para querys de cantante.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public static final String queryInsertCantante = "insert into Cantante (Nombre, Apellido) values (?, ?)";
	public static final String queryConsultaCantante = "SELECT * FROM lando.cantante;";

	/**
	 * Constantes para querys de cancion.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public static final String queryInsertCancion = "insert into Canciones (Libreria_Multimedia_idLibreria_Multimedia, T�tulo, T�tulo_original, A�o, Tipo_DoA, Precio, En_propiedad, En_busqueda, Formato, Enlace_a_YT, G�nero_Canci�n_idG�nero_Canci�n, Album_idAlbum, Album_Cantante_idCantante)  values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
	public static final String queryConsultaCancion = "SELECT * FROM lando.canciones";

	/**
	 * Constantes para querys de albumes de canciones.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public static final String queryInsertAlbum = "insert into Album (Nombre, Cantante_idCantante) values (?, ?)";
	public static final String queryConsultaAlbum = "SELECT * FROM lando.album;";

	/**
	 * Constantes para querys de genero de canciones.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static final String queryInsertGeneroCancion = "insert into G�nero_Canci�n (Nombre) values (?)";
	public static final String queryConsultaGeneroCancion = "SELECT * FROM lando.g�nero_canci�n;";

	/**
	 * Constantes para querys de premios de canciones.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static final String queryInsertPremiosCancion = "insert into Premios_Canci�n (Nombre, Categoria, A�o, Canciones_idCanciones, Canciones_G�nero_Canci�n_idG�nero_Canci�n, Canciones_Album_idAlbum, Canciones_Album_Cantante_idCantante, Canciones_Libreria_Multimedia_idLibreria_Multimedia) values (?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String queryConsultaPremiosCancion = "SELECT * FROM lando.premios_canci�n;";

}