package COMUN;

/**
 * Esta es la interfaz para comunicaciones entre las capas LN y LD.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public interface itfDatos {

	/**
	 * Esta es el metodo para transferir información entre LD y LN.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E3
	 */

	Object getData(String columna);

}
