package LD;

import java.util.HashMap;

import COMUN.itfDatos;

/**
 * Esta clase era para el hasmap pero no se usa.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */
public class clsFila implements itfDatos {

	private HashMap<String, Object> fila;

	public clsFila() {
		fila = new HashMap<String, Object>();
	}

	public void ponerColumna(String columna, Object valor) {
		fila.put(columna, valor);
	}

	public Object getData(String columna) {

		return this.fila.get(columna);
	}

}
