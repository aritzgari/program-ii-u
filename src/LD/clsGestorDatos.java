package LD;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import COMUN.itfDatos;
import COMUN.itfProperty;
import COMUN.Constantes;
import LD.clsFila;
import LN.clsCancionAlbum;
import LD.clsAlbumBD;
import LD.clsCantanteBD;

/**
 * Esta es el gestor para gestionar la informaci�n entre LD y LN.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsGestorDatos implements itfDatos {

	/**
	 * Este metodo sirve para crear cantantes en BD.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static int crearCantante(String Nombre, String Apellido) {
		int retorno = 0;
		clsCantanteBD objCantanteBD;
		objCantanteBD = new clsCantanteBD(Nombre, Apellido);
		retorno = objCantanteBD.sendInsert(Constantes.queryInsertCantante);
		System.out.println(retorno);
		return retorno;
	}

	/**
	 * Este metodo sirve para crear canciones en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static int crearCancion(int Libreria_Multimedia_idLibreria_Multimedia, String titulo, String titulo_original,
			int anno_de_publicacion, int idGenero, int idAlbum, int idCantante, String tipo_DoA, double precio,
			boolean en_propiedad, boolean en_busqueda, String formato, String Enlace_a_YT) {
		int retorno = 0;
		clsCancionBD objCancionBD;
		objCancionBD = new clsCancionBD(Libreria_Multimedia_idLibreria_Multimedia, titulo, titulo_original,
				anno_de_publicacion, tipo_DoA, precio, en_propiedad, en_busqueda, formato, Enlace_a_YT, idGenero,
				idAlbum, idCantante);
		retorno = objCancionBD.sendInsert(Constantes.queryInsertCancion);
		System.out.println(retorno);
		return retorno;
	}

	/**
	 * Este metodo sirve para crear albumes en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static int crearAlbum(String Nombre, int Cantante_idCantante) {
		int retorno = 0;
		clsAlbumBD objAlbumBD = new clsAlbumBD(Nombre, Cantante_idCantante);
		retorno = objAlbumBD.sendInsert(Constantes.queryInsertAlbum);
		System.out.println(retorno);
		return retorno;
	}

	/**
	 * Este metodo sirve para crear generos de canciones en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public static int crearGeneroCancion(String Nombre) {
		int retorno = 0;
		clsGeneroCancionBD objGeneroCancionBD;
		objGeneroCancionBD = new clsGeneroCancionBD(Nombre);
		retorno = objGeneroCancionBD.sendInsert(Constantes.queryInsertGeneroCancion);
		System.out.println(retorno);
		return retorno;
	}

	/**
	 * Este metodo sirve para crear premios de canciones en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static int crearPremiosCancion(String Nombre, String Categoria, int A�o, int Canciones_idCanciones,
			int Canciones_G�nero_Canci�n_idG�nero_Canci�n, int Canciones_Album_idAlbum,
			int Canciones_Album_Cantante_idCantante, int Canciones_Libreria_Multimedia_idLibreria_Multimedia) {
		int retorno = 0;
		clsPremios_CancionBD objPremiosCancionBD = new clsPremios_CancionBD(Nombre, Categoria, A�o,
				Canciones_idCanciones, Canciones_G�nero_Canci�n_idG�nero_Canci�n, Canciones_Album_idAlbum,
				Canciones_Album_Cantante_idCantante, Canciones_Libreria_Multimedia_idLibreria_Multimedia);
		retorno = objPremiosCancionBD.sendInsert(Constantes.queryInsertPremiosCancion);
		System.out.println(retorno);
		return retorno;
	}

	/**
	 * Este metodo sirve para crear librerias multimedia en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public static int crearLibreriaMultimedia(String nombre, String descripcion) {
		int retorno = 0;

		clsLibreria_MultimediaBD objlibreriamultimediaBD = new clsLibreria_MultimediaBD(nombre, descripcion);

		retorno = objlibreriamultimediaBD.sendInsert(Constantes.queryInsertLibreria);

		System.out.println(retorno);

		return retorno;
	}

	@Override
	public Object getData(String columna) {
		// TODO Auto-generated method stub
		return null;
	}

}
