import java.awt.EventQueue;

import LP.clsMenu;
import LP.frmMenu;

/**
 * Clase Main que inicia la magia.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E1
 */

public class main {

	public static void main(String[] args) {

		frmMenu v1 = new frmMenu();
		v1.setVisible(true);
		v1.setResizable(false);

	}

}
