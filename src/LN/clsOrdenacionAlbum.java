package LN;

import java.util.Comparator;

/**
 * Esta clase es para ordenacion de albumes
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E4
 */

public class clsOrdenacionAlbum implements Comparator<clsCancionAlbum> {

	@Override
	public int compare(clsCancionAlbum arg0, clsCancionAlbum arg1) {
		// TODO Auto-generated method stub
		return arg0.getNombre().compareTo(arg1.getNombre());
	}

}
