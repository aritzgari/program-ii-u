package LN;

/**
 * clase de la libreria multimedia
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsLibreriaMultimedia implements Comparable {

	private int idLibreriaMultimedia;
	private String Nombre;
	private String Descripcion;

	public clsLibreriaMultimedia(int _idLibreria_Multimedia, String _Nombre, String _Descripcion) {
		idLibreriaMultimedia = _idLibreria_Multimedia;
		Nombre = _Nombre;
		Descripcion = _Descripcion;
	}

	/**
	 * @return the idLibreriaMultimedia
	 */
	public int getIdLibreriaMultimedia() {
		return idLibreriaMultimedia;
	}

	/**
	 * @param idLibreriaMultimedia the idLibreriaMultimedia to set
	 */
	public void setIdLibreriaMultimedia(int idLibreriaMultimedia) {
		this.idLibreriaMultimedia = idLibreriaMultimedia;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return Descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	@Override
	public int compareTo(Object arg0) {

		String nom;
		clsLibreriaMultimedia objCast;

		nom = Nombre;

		if (arg0 == null)
			throw new NullPointerException();
		if (arg0.getClass() != this.getClass())
			throw new ClassCastException();

		objCast = (clsLibreriaMultimedia) arg0;
		// TODO Auto-generated method stub
		return nom.compareTo(objCast.getNombre());
	}

	@Override
	public String toString() {
		return "ID=" + idLibreriaMultimedia + ", Nombre=" + Nombre
				+ ", Descripcion=" + Descripcion;
	}
	

}
