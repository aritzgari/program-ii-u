package LN;

/**
 * Esta clase servira para a�adir actores de peliculas
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsPeliculaActores {
	private int idActores;
	private String Nombre;
	private String Apellido;

	public clsPeliculaActores(int _idActor, String _Nombre, String _Apellido) {
		// TODO Auto-generated constructor stub
		idActores = _idActor;
		Nombre = _Nombre;
		Apellido = _Apellido;
	}

	/**
	 * @return the idActores
	 */
	public int getIdActores() {
		return idActores;
	}

	/**
	 * @param idActores the idActores to set
	 */
	public void setIdActores(int idActores) {
		this.idActores = idActores;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return Apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		Apellido = apellido;
	}

}
