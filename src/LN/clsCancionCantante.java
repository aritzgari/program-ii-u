package LN;

/**
 * Esta clase servira para a�adir cantante de cancion
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsCancionCantante implements Comparable {
	
	private int idCantante;
	private String Nombre;
	private String Apellido;

	public clsCancionCantante(int idCantante, String nombre, String apellido) {
		super();
		this.idCantante = idCantante;
		Nombre = nombre;
		Apellido = apellido;
	}

	public clsCancionCantante() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the idCantante
	 */
	public int getIdCantante() {
		return idCantante;
	}

	/**
	 * @param idCantante the idCantante to set
	 */
	public void setIdCantante(int idCantante) {
		this.idCantante = idCantante;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return Apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int compareTo(Object arg0) {

		String nom;
		clsCancionCantante objCast;

		nom = Nombre;

		if (arg0 == null)
			throw new NullPointerException();
		if (arg0.getClass() != this.getClass())
			throw new ClassCastException();

		objCast = (clsCancionCantante) arg0;
		// TODO Auto-generated method stub
		return nom.compareTo(objCast.getNombre());
	}
	
	@Override
	public String toString() {
		return "" + Nombre + "";
	}


}
