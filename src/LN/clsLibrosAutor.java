package LN;

/**
 * Esta clase servira para a�adir autores de libros
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsLibrosAutor {

	private int idAutor;
	private String Nombre;
	private String Apellido;

	public clsLibrosAutor(int _idAutor, String _Nombre, String _Apellido) {
		// TODO Auto-generated constructor stub
		setIdAutor(_idAutor);
		setNombre(_Nombre);
		setApellido(_Apellido);
	}

	/**
	 * @return the idAutor
	 */
	public int getIdAutor() {
		return idAutor;
	}

	/**
	 * @param idAutor the idAutor to set
	 */
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return Apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

}
