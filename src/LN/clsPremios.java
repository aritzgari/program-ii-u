package LN;

/**
 * Esta clase es padre de clsPeliculaPremios, clsLibrosPremios y
 * clsCancionPremios
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public abstract class clsPremios {
	private int idPremios;
	private String Nombre;
	private String Categoria;
	private int A�o;

	public clsPremios() {
		/* constructor vacio */
	}

	/**
	 * @return the idPremios
	 */
	public int getIdPremios() {
		return idPremios;
	}

	/**
	 * @param idPremios the idPremios to set
	 */
	public void setIdPremios(int idPremios) {
		this.idPremios = idPremios;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	/**
	 * @return the categoria
	 */
	public String getCategoria() {
		return Categoria;
	}

	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(String categoria) {
		Categoria = categoria;
	}

	/**
	 * @return the a�o
	 */
	public int getA�o() {
		return A�o;
	}

	/**
	 * @param a�o the a�o to set
	 */
	public void setA�o(int a�o) {
		A�o = a�o;
	}

}
