package LN;

/**
 * Esta clase es la clase hija de premios, servira para a�adir premios de
 * canciones
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsCancionPremios extends clsPremios {
	private int Canciones_idCanciones;
	private int Canciones_G�nero_Canci�n_idG�nero_Canci�n;
	private int Canciones_Album_idAlbum;
	private int Canciones_Album_Cantante_idCantante;
	private int Canciones_Libreria_Multimedia_idLibreria_Multimedia;

	public clsCancionPremios(int _idPremios, String _nombre, String _categoria, int _anno, int _Canciones_idCanciones,
			int _Canciones_G�nero_Canci�n_idG�nero_Canci�n, int _Canciones_Album_idAlbum,
			int _Canciones_Album_Cantante_idCantante, int _Canciones_Libreria_Multimedia_idLibreria_Multimedia) {
		// TODO Auto-generated constructor stub
		setCanciones_Album_Cantante_idCantante(_Canciones_Album_Cantante_idCantante);
		setCanciones_G�nero_Canci�n_idG�nero_Canci�n(_Canciones_G�nero_Canci�n_idG�nero_Canci�n);
		setCanciones_Album_idAlbum(_Canciones_Album_idAlbum);
		setCanciones_Album_Cantante_idCantante(_Canciones_Album_Cantante_idCantante);
		setCanciones_Libreria_Multimedia_idLibreria_Multimedia(_Canciones_Libreria_Multimedia_idLibreria_Multimedia);
		setIdPremios(_idPremios);
		setNombre(_nombre);
		setCategoria(_categoria);
		setA�o(_anno);
	}

	/**
	 * @return the canciones_Album_idAlbum
	 */
	public int getCanciones_Album_idAlbum() {
		return Canciones_Album_idAlbum;
	}

	/**
	 * @param canciones_Album_idAlbum the canciones_Album_idAlbum to set
	 */
	public void setCanciones_Album_idAlbum(int canciones_Album_idAlbum) {
		Canciones_Album_idAlbum = canciones_Album_idAlbum;
	}

	/**
	 * @return the canciones_idCanciones
	 */
	public int getCanciones_idCanciones() {
		return Canciones_idCanciones;
	}

	/**
	 * @param canciones_idCanciones the canciones_idCanciones to set
	 */
	public void setCanciones_idCanciones(int canciones_idCanciones) {
		Canciones_idCanciones = canciones_idCanciones;
	}

	/**
	 * @return the canciones_G�nero_Canci�n_idG�nero_Canci�n
	 */
	public int getCanciones_G�nero_Canci�n_idG�nero_Canci�n() {
		return Canciones_G�nero_Canci�n_idG�nero_Canci�n;
	}

	/**
	 * @param canciones_G�nero_Canci�n_idG�nero_Canci�n the
	 *                                                  canciones_G�nero_Canci�n_idG�nero_Canci�n
	 *                                                  to set
	 */
	public void setCanciones_G�nero_Canci�n_idG�nero_Canci�n(int canciones_G�nero_Canci�n_idG�nero_Canci�n) {
		Canciones_G�nero_Canci�n_idG�nero_Canci�n = canciones_G�nero_Canci�n_idG�nero_Canci�n;
	}

	/**
	 * @return the canciones_Libreria_Multimedia_idLibreria_Multimedia
	 */
	public int getCanciones_Libreria_Multimedia_idLibreria_Multimedia() {
		return Canciones_Libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @param canciones_Libreria_Multimedia_idLibreria_Multimedia the
	 *                                                            canciones_Libreria_Multimedia_idLibreria_Multimedia
	 *                                                            to set
	 */
	public void setCanciones_Libreria_Multimedia_idLibreria_Multimedia(
			int canciones_Libreria_Multimedia_idLibreria_Multimedia) {
		Canciones_Libreria_Multimedia_idLibreria_Multimedia = canciones_Libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @return the canciones_Album_Cantante_idCantante
	 */
	public int getCanciones_Album_Cantante_idCantante() {
		return Canciones_Album_Cantante_idCantante;
	}

	/**
	 * @param canciones_Album_Cantante_idCantante the
	 *                                            canciones_Album_Cantante_idCantante
	 *                                            to set
	 */
	public void setCanciones_Album_Cantante_idCantante(int canciones_Album_Cantante_idCantante) {
		Canciones_Album_Cantante_idCantante = canciones_Album_Cantante_idCantante;
	}

}
