package LN;

/**
 * Esta clase es la clase hija de clsGeneros, servira para a�adir generos de
 * libros
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsLibrosGenero extends clsGeneros {

	public clsLibrosGenero(int _idGenero, String _Nombre) {
		// TODO Auto-generated constructor stub
		setIdGenero(_idGenero);
		setNombre(_Nombre);
	}

}
