
package LN;

/**
 * Esta clase es la clase hija de articulo, sera para a�adir libros
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsLibros extends clsArticulo {

	private int Libreria_Multimedia_idLibreria_Multimedia;
	private String Resumen;
	private boolean Serie_SoN;
	private String Nombre_serie;
	private double Orden_serie;
	private String ISBN;
	private int Paginas;
	private int idGenero;
	private int idAutor;
	private int idEditorial;

	public clsLibros(int _libreria_Multimedia_idLibreria_Multimedia, String _ISBN, String _titulo,
			String _titulo_original, int _anno_de_publicacion, String _tipo_DoA, double _precio, boolean _en_propiedad,
			boolean _en_busqueda, String _formato, int _Paginas, String _Resumen, boolean _Serie_SoN,
			String _Nombre_serie, double _Orden_serie, int _idGenero, int _idAutor, int _idEditorial) {
		// TODO Auto-generated constructor stub
		setLibreria_Multimedia_idLibreria_Multimedia(_libreria_Multimedia_idLibreria_Multimedia);
		setResumen(_Resumen);
		setSerie_SoN(_Serie_SoN);
		setNombre_serie(_Nombre_serie);
		setOrden_serie(_Orden_serie);
		setISBN(_ISBN);
		setPaginas(_Paginas);
		setIdGenero(_idGenero);
		setIdAutor(_idAutor);
		setIdEditorial(_idEditorial);
		setPaginas(_Paginas);
		setTitulo(_titulo);
		setTitulo_original(_titulo_original);
		setAnno_de_publicacion(_anno_de_publicacion);
		setTipo_DoA(_tipo_DoA);
		setPrecio(_precio);
		setEn_propiedad(_en_propiedad);
		setEn_busqueda(_en_busqueda);
		setFormato(_formato);

	}

	/**
	 * @return the libreria_Multimedia_idLibreria_Multimedia
	 */
	public int getLibreria_Multimedia_idLibreria_Multimedia() {
		return Libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @param libreria_Multimedia_idLibreria_Multimedia the
	 *                                                  libreria_Multimedia_idLibreria_Multimedia
	 *                                                  to set
	 */
	public void setLibreria_Multimedia_idLibreria_Multimedia(int libreria_Multimedia_idLibreria_Multimedia) {
		Libreria_Multimedia_idLibreria_Multimedia = libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @return the resumen
	 */
	public String getResumen() {
		return Resumen;
	}

	/**
	 * @param resumen the resumen to set
	 */
	public void setResumen(String resumen) {
		Resumen = resumen;
	}

	/**
	 * @return the nombre_serie
	 */
	public String getNombre_serie() {
		return Nombre_serie;
	}

	/**
	 * @param nombre_serie the nombre_serie to set
	 */
	public void setNombre_serie(String nombre_serie) {
		Nombre_serie = nombre_serie;
	}

	/**
	 * @return the serie_SoN
	 */
	public boolean isSerie_SoN() {
		return Serie_SoN;
	}

	/**
	 * @param serie_SoN the serie_SoN to set
	 */
	public void setSerie_SoN(boolean serie_SoN) {
		Serie_SoN = serie_SoN;
	}

	/**
	 * @return the iSBN
	 */
	public String getISBN() {
		return ISBN;
	}

	/**
	 * @param iSBN the iSBN to set
	 */
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	/**
	 * @return the orden_serie
	 */
	public double getOrden_serie() {
		return Orden_serie;
	}

	/**
	 * @param orden_serie the orden_serie to set
	 */
	public void setOrden_serie(double orden_serie) {
		Orden_serie = orden_serie;
	}

	/**
	 * @return the idGenero
	 */
	public int getIdGenero() {
		return idGenero;
	}

	/**
	 * @param idGenero the idGenero to set
	 */
	public void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	/**
	 * @return the paginas
	 */
	public int getPaginas() {
		return Paginas;
	}

	/**
	 * @param paginas the paginas to set
	 */
	public void setPaginas(int paginas) {
		Paginas = paginas;
	}

	/**
	 * @return the idAutor
	 */
	public int getIdAutor() {
		return idAutor;
	}

	/**
	 * @param idAutor the idAutor to set
	 */
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}

	/**
	 * @return the idEditorial
	 */
	public int getIdEditorial() {
		return idEditorial;
	}

	/**
	 * @param idEditorial the idEditorial to set
	 */
	public void setIdEditorial(int idEditorial) {
		this.idEditorial = idEditorial;
	}
}
