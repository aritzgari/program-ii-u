package LN;

/**
 * Esta clase es padre de clsPeliculaGenero, clsLibrosGenero y clsCancionGenero
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public abstract class clsGeneros {

	private int idGenero;
	private String Nombre;

	public clsGeneros() {
		/* constructor vacio */
	}

	/**
	 * @return the idGenero
	 */
	public int getIdGenero() {
		return idGenero;
	}

	/**
	 * @param idGenero the idGenero to set
	 */
	public void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

}
