package LN;

/**
 * Esta clase es la padre de pel�cula, cancion y libro
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public abstract class clsArticulo {

	private String Titulo;
	private String Titulo_original;
	private int Anno_de_publicacion;
	private String Tipo_DoA;
	private String Formato;
	private boolean En_propiedad;
	private boolean En_busqueda;
	private double Precio;
	private String Genero;
	private String Premios;

	public clsArticulo() {
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return Titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		Titulo = titulo;
	}

	/**
	 * @return the titulo_original
	 */
	public String getTitulo_original() {
		return Titulo_original;
	}

	/**
	 * @param titulo_original the titulo_original to set
	 */
	public void setTitulo_original(String titulo_original) {
		Titulo_original = titulo_original;
	}

	/**
	 * @return the anno_de_publicacion
	 */
	public int getAnno_de_publicacion() {
		return Anno_de_publicacion;
	}

	/**
	 * @param anno_de_publicacion the anno_de_publicacion to set
	 */
	public void setAnno_de_publicacion(int anno_de_publicacion) {
		Anno_de_publicacion = anno_de_publicacion;
	}

	/**
	 * @return the tipo_DoA
	 */
	public String getTipo_DoA() {
		return Tipo_DoA;
	}

	/**
	 * @param tipo_DoA the tipo_DoA to set
	 */
	public void setTipo_DoA(String tipo_DoA) {
		Tipo_DoA = tipo_DoA;
	}

	/**
	 * @return the en_propiedad
	 */
	public boolean isEn_propiedad() {
		return En_propiedad;
	}

	/**
	 * @param en_propiedad the en_propiedad to set
	 */
	public void setEn_propiedad(boolean en_propiedad) {
		En_propiedad = en_propiedad;
	}

	/**
	 * @return the formato
	 */
	public String getFormato() {
		return Formato;
	}

	/**
	 * @param formato the formato to set
	 */
	public void setFormato(String formato) {
		Formato = formato;
	}

	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return Precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		Precio = precio;
	}

	/**
	 * @return the en_busqueda
	 */
	public boolean isEn_busqueda() {
		return En_busqueda;
	}

	/**
	 * @param en_busqueda the en_busqueda to set
	 */
	public void setEn_busqueda(boolean en_busqueda) {
		En_busqueda = en_busqueda;
	}

	/**
	 * @return the premios
	 */
	public String getPremios() {
		return Premios;
	}

	/**
	 * @param premios the premios to set
	 */
	public void setPremios(String premios) {
		Premios = premios;
	}

	/**
	 * @return the genero
	 */
	public String getGenero() {
		return Genero;
	}

	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		Genero = genero;
	}
}
