package LN;

/**
 * Esta clase servira para a�adir directores de peliculas
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsPeliculaDirector {
	private int idDirector;
	private String Nombre;
	private String Apellido;

	public clsPeliculaDirector(int _idDirector, String _Nombre, String _Apellido) {
		// TODO Auto-generated constructor stub
		setIdDirector(_idDirector);
		setNombre(_Nombre);
		setApellido(_Apellido);

	}

	/**
	 * @return the idDirector
	 */
	public int getIdDirector() {
		return idDirector;
	}

	/**
	 * @param idDirector the idDirector to set
	 */
	public void setIdDirector(int idDirector) {
		this.idDirector = idDirector;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return Apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		Apellido = apellido;
	}

}
