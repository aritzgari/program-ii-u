package LN;

/**
 * Esta clase servira para a�adir editorial de libros
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsLibrosEditorial {

	private int idEditorial;
	private String Nombre;

	public clsLibrosEditorial(int _idEditorial, String _Nombre) {
		// TODO Auto-generated constructor stub
		setIdEditorial(_idEditorial);
		setNombre(_Nombre);
	}

	/**
	 * @return the idEditorial
	 */
	public int getIdEditorial() {
		return idEditorial;
	}

	/**
	 * @param idEditorial the idEditorial to set
	 */
	public void setIdEditorial(int idEditorial) {
		this.idEditorial = idEditorial;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

}
