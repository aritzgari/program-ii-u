package LN;

/**
 * Esta clase es la clase hija de articulo, sera para a�adir libros
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsPelicula extends clsArticulo {

	private int Libreria_Multimedia_idLibreria_Multimedia;
	private int Duracion;
	private int Puntuaci�n;
	private int Calporedad;
	private String Saga;
	private double Orden;
	private String Sinopsis;
	private String Enlace_a_trailer;

	public clsPelicula(int _libreria_Multimedia_idLibreria_Multimedia, String _titulo, String _titulo_original,
			int _anno_de_publicacion, int _duracion, int _puntuacion, int _calporedad, String _tipo_DoA, double _precio,
			boolean _en_propiedad, boolean _en_busqueda, String _formato, String _saga, Double _orden, String _sinopsis,
			String _enlace_a_trailer) {
		// TODO Auto-generated constructor stub
		setLibreria_Multimedia_idLibreria_Multimedia(_libreria_Multimedia_idLibreria_Multimedia);
		setDuracion(_duracion);
		setPuntuaci�n(_puntuacion);
		setCalporedad(_calporedad);
		setSaga(_saga);
		setOrden(_orden);
		setSinopsis(_sinopsis);
		setEnlace_a_trailer(_enlace_a_trailer);
		setTitulo(_titulo);
		setTitulo_original(_titulo_original);
		setAnno_de_publicacion(_anno_de_publicacion);
		setTipo_DoA(_tipo_DoA);
		setPrecio(_precio);
		setEn_propiedad(_en_propiedad);
		setEn_busqueda(_en_busqueda);
		setFormato(_formato);
	}

	/**
	 * @return the libreria_Multimedia_idLibreria_Multimedia
	 */
	public int getLibreria_Multimedia_idLibreria_Multimedia() {
		return Libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @param libreria_Multimedia_idLibreria_Multimedia the
	 *                                                  libreria_Multimedia_idLibreria_Multimedia
	 *                                                  to set
	 */
	public void setLibreria_Multimedia_idLibreria_Multimedia(int libreria_Multimedia_idLibreria_Multimedia) {
		Libreria_Multimedia_idLibreria_Multimedia = libreria_Multimedia_idLibreria_Multimedia;
	}

	/**
	 * @return the calporedad
	 */
	public int getCalporedad() {
		return Calporedad;
	}

	/**
	 * @param calporedad the calporedad to set
	 */
	public void setCalporedad(int calporedad) {
		Calporedad = calporedad;
	}

	/**
	 * @return the puntuaci�n
	 */
	public int getPuntuaci�n() {
		return Puntuaci�n;
	}

	/**
	 * @param puntuaci�n the puntuaci�n to set
	 */
	public void setPuntuaci�n(int puntuaci�n) {
		Puntuaci�n = puntuaci�n;
	}

	/**
	 * @return the duracion
	 */
	public int getDuracion() {
		return Duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(int duracion) {
		Duracion = duracion;
	}

	/**
	 * @return the orden
	 */
	public double getOrden() {
		return Orden;
	}

	/**
	 * @param orden the orden to set
	 */
	public void setOrden(double orden) {
		Orden = orden;
	}

	/**
	 * @return the saga
	 */
	public String getSaga() {
		return Saga;
	}

	/**
	 * @param saga the saga to set
	 */
	public void setSaga(String saga) {
		Saga = saga;
	}

	/**
	 * @return the sinopsis
	 */
	public String getSinopsis() {
		return Sinopsis;
	}

	/**
	 * @param sinopsis the sinopsis to set
	 */
	public void setSinopsis(String sinopsis) {
		Sinopsis = sinopsis;
	}

	/**
	 * @return the enlace_a_trailer
	 */
	public String getEnlace_a_trailer() {
		return Enlace_a_trailer;
	}

	/**
	 * @param enlace_a_trailer the enlace_a_trailer to set
	 */
	public void setEnlace_a_trailer(String enlace_a_trailer) {
		Enlace_a_trailer = enlace_a_trailer;
	}

}
