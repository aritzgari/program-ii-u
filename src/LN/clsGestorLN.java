package LN;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import COMUN.itfDatos;
import COMUN.itfProperty;
import COMUN.Constantes;
import LN.clsCancionAlbum;
import LD.clsAlbumBD;
import LD.clsCancionBD;
import LD.clsCantanteBD;
import LD.clsGeneroCancionBD;
import LD.clsGestorDatos;
import LD.clsLibreria_MultimediaBD;
import LD.clsPremios_CancionBD;
import COMUN.itfProperty;
import LN.clsCancionAlbum;
import jdk.nashorn.api.tree.ForInLoopTree;

/**
 * Esta clase es para el gestor del paquete LN
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsGestorLN implements itfDatos, itfProperty {

	/**
	 * Objetos para la inserción en capa LD.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	private clsGestorDatos objcantante = new clsGestorDatos();

	private clsGestorDatos objconsultacantante = new clsGestorDatos();

	private clsGestorDatos objalbum = new clsGestorDatos();

	private clsGestorDatos objcancion = new clsGestorDatos();

	private clsGestorDatos objgenerocancion = new clsGestorDatos();

	private clsGestorDatos objpremiocancion = new clsGestorDatos();

	private clsGestorDatos objLibreriaMultimedia = new clsGestorDatos();

	private clsGestorDatos objCantante = new clsGestorDatos();

	private clsGestorDatos objPremiosCancion = new clsGestorDatos();
	/**
	 * Arraylists para la consulta capa LN.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	private static ArrayList<clsCancionCantante> datosCantante;
	private static ArrayList<clsCancionAlbum> datosAlbum;
	private static ArrayList<clsLibreriaMultimedia> datosLibreria;
	private static ArrayList<clsCancionGenero> datosCancionGenero;
	private static ArrayList<clsCancion> datosCancion;
	private static ArrayList<clsCancionPremios> datosCancionPremios;

	/**
	 * Arraylist de itfproperty.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	private static ArrayList<itfProperty> datosItf;

	public clsGestorLN() {
		datosItf = new ArrayList<itfProperty>();
		datosCantante = new ArrayList<clsCancionCantante>();
		datosAlbum = new ArrayList<clsCancionAlbum>();
		datosLibreria = new ArrayList<clsLibreriaMultimedia>();
		datosCancionGenero = new ArrayList<clsCancionGenero>();
		datosCancion = new ArrayList<clsCancion>();
		datosCancionPremios = new ArrayList<clsCancionPremios>();
	}

	/**
	 * Este metodo sirve relacionar la informacion de inserciones de cantantes entre
	 * LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public void crearcantante(String nombre, String apellido) {
		// TODO Auto-generated method stub

		objcantante.crearCantante(nombre, apellido);

	}

	/**
	 * Este metodo sirve relacionar la informacion de insercion de canciones entre
	 * LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public void crearcancion(int Libreria_Multimedia_idLibreria_Multimedia, String titulo, String titulo_original,
			int anno_de_publicacion, int idGenero, int idAlbum, int idCantante, String tipo_DoA, double precio,
			boolean en_propiedad, boolean en_busqueda, String formato, String Enlace_a_YT) {

		objcancion.crearCancion(Libreria_Multimedia_idLibreria_Multimedia, titulo, titulo_original, anno_de_publicacion,
				idGenero, idAlbum, idCantante, tipo_DoA, precio, en_propiedad, en_busqueda, formato, Enlace_a_YT);
	}

	/**
	 * Este metodo sirve relacionar la informacion de inserciones de albumes entre
	 * LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public void crearalbum(int id_cantante, String nombre) {

		objalbum.crearAlbum(nombre, id_cantante);
	}

	/**
	 * Este metodo sirve relacionar la informacion de inserciones de generos de
	 * canciones entre LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public void creargenerocancion(String nombre) {

		objgenerocancion.crearGeneroCancion(nombre);

	}

	/**
	 * Este metodo sirve relacionar la informacion de inserciones de premios de
	 * canciones entre LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */

	public void crearpremiocancion(String nombre, String categoria, int annio, int idcancion, int idgenerocancion,
			int idalbum, int idcantante, int idLibreriaMultimedia) {

		objpremiocancion.crearPremiosCancion(nombre, categoria, annio, idcancion, idgenerocancion, idalbum, idcantante,
				idLibreriaMultimedia);

	}

	/**
	 * Este metodo sirve relacionar la informacion de inserciones de librerias
	 * multimedia entre LD y LP.
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public void crearaLibreriaMultimedia(String nombre, String descripcion) {
		objLibreriaMultimedia.crearLibreriaMultimedia(nombre, descripcion);
	}

	/**
	 * Este metodo sirve consultar los cantantes en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public ArrayList<clsCancionCantante> consultarCantanteEnBD() {
		ResultSet resultado = null;
		clsCantanteBD objCantante = new clsCantanteBD();

		objCantante.conectarBD();
		resultado = objCantante.sendSelect(Constantes.queryConsultaCantante);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsCancionCantante objcantante2 = new clsCancionCantante(resultado.getInt(1), resultado.getString(2),
						resultado.getString(3));
				datosCantante.add(objcantante2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objCantante.desconectarBD(objCantante.getObjCon());
		return datosCantante;
	}

	/**
	 * Este metodo sirve consultar los albumes en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public ArrayList<clsCancionAlbum> consultarAlbumEnBD() {
		ResultSet resultado = null;
		clsAlbumBD objAlbum = new clsAlbumBD();

		objAlbum.conectarBD();
		resultado = objAlbum.sendSelect(Constantes.queryConsultaAlbum);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsCancionAlbum objAlbum2 = new clsCancionAlbum(resultado.getInt(1), resultado.getInt(3),
						resultado.getString(2));
				datosAlbum.add(objAlbum2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objAlbum.desconectarBD(objAlbum.getObjCon());
		return datosAlbum;
	}

	/**
	 * Este metodo sirve consultar los cantantes en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E4
	 */
	public ArrayList<clsLibreriaMultimedia> consultarLibreriaMultimediaEnBD() {
		ResultSet resultado = null;
		clsLibreria_MultimediaBD objLibreria = new clsLibreria_MultimediaBD();

		objLibreria.conectarBD();
		resultado = objLibreria.sendSelect(Constantes.queryConsultaLibreria);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsLibreriaMultimedia objLibreria2 = new clsLibreriaMultimedia(resultado.getInt(1),
						resultado.getString(2), resultado.getString(3));
				datosLibreria.add(objLibreria2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objLibreria.desconectarBD(objLibreria.getObjCon());
		return datosLibreria;
	}

	/**
	 * Este metodo sirve consultar los generos en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E5
	 */
	public ArrayList<clsCancionGenero> consultarCancionGeneroEnBD() {
		ResultSet resultado = null;
		clsGeneroCancionBD objGenero = new clsGeneroCancionBD();

		objGenero.conectarBD();
		resultado = objGenero.sendSelect(Constantes.queryConsultaGeneroCancion);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsCancionGenero objgenero2 = new clsCancionGenero(resultado.getInt(1), resultado.getString(2));
				datosCancionGenero.add(objgenero2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objGenero.desconectarBD(objGenero.getObjCon());
		return datosCancionGenero;
	}

	/**
	 * Este metodo sirve consultar los generos en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E5
	 */
	public ArrayList<clsCancionPremios> consultarPremiosCancionEnBD() {
		ResultSet resultado = null;
		clsPremios_CancionBD objPremios = new clsPremios_CancionBD();

		objPremios.conectarBD();
		resultado = objPremios.sendSelect(Constantes.queryConsultaPremiosCancion);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsCancionPremios objcancion2 = new clsCancionPremios(resultado.getInt(1), resultado.getString(2),
						resultado.getString(3), resultado.getInt(4), resultado.getInt(5), resultado.getInt(6),
						resultado.getInt(7), resultado.getInt(8), resultado.getInt(9));
				datosCancionPremios.add(objcancion2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objPremios.desconectarBD(objPremios.getObjCon());
		return datosCancionPremios;
	}

	/**
	 * Este metodo sirve consultar las canciones en BD
	 * 
	 * @author Aritz Garitano y Iker Reyes
	 * @since E5
	 */
	public ArrayList<clsCancion> consultarCancionEnBD() {
		ResultSet resultado = null;
		clsCancionBD objCancion = new clsCancionBD();

		objCancion.conectarBD();
		resultado = objCancion.sendSelect(Constantes.queryConsultaCancion);
		// Meto rs en objeto
		try {
			while (resultado.next()) {
				clsCancion objcancion2 = new clsCancion(resultado.getInt(1), resultado.getString(2),
						resultado.getString(3), resultado.getInt(4), resultado.getString(5), resultado.getInt(6),
						resultado.getBoolean(7), resultado.getBoolean(8), resultado.getString(9),
						resultado.getString(10), resultado.getInt(11), resultado.getInt(12), resultado.getInt(13),
						resultado.getInt(14));
				datosCancion.add(objcancion2);
			}
		} catch (SQLException e) {
			System.out.println("Error SQL");
			System.out.println(e);
		}
		// Cierro conexion
		objCancion.desconectarBD(objCancion.getObjCon());
		return datosCancion;
	}

	@Override
	public Object getData(String columna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getProperty(String columna) {
		// TODO Auto-generated method stub
		return null;
	}

}
