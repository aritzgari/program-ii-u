package LN;

import java.util.ArrayList; 
import java.util.Comparator;

/**
 * Esta clase es para ordenacion de cantante
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E4
 */

public class clsOrdenacionCantante implements Comparator<clsCancionCantante> {

	
	public int compare(clsCancionCantante A1, clsCancionCantante A2) {
		if (A1.getNombre().compareTo(A2.getNombre()) == 0) {
			return A1.getApellido().compareTo(A2.getApellido());
		} else {
			return A1.getNombre().compareTo(A2.getNombre());
		}
	}
	
}
