package LN;

/**
 * Esta clase es la clase hija de premios, servira para a�adir premios de
 * peliculas
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsPeliculaPremios extends clsPremios {
	private String titulodepelicula;

	public clsPeliculaPremios(int _idpremios, String _Nombre, String _Categoria, int _A�o, String _Titulodepelicula) {
		// TODO Auto-generated constructor stub
		setIdPremios(_idpremios);
		setNombre(_Nombre);
		setCategoria(_Categoria);
		setA�o(_A�o);
		setTitulodepelicula(_Titulodepelicula);
	}

	/**
	 * @return the titulodepelicula
	 */
	public String getTitulodepelicula() {
		return titulodepelicula;
	}

	/**
	 * @param titulodepelicula the titulodepelicula to set
	 */
	public void setTitulodepelicula(String titulodepelicula) {
		this.titulodepelicula = titulodepelicula;
	}

}
