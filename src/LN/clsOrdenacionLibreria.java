package LN;

import java.util.Comparator;

/**
 * Esta clase es para ordenacion de 
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E4
 */

public class clsOrdenacionLibreria implements Comparator<clsLibreriaMultimedia> {

	@Override
	public int compare(clsLibreriaMultimedia arg0, clsLibreriaMultimedia arg1) {
		// TODO Auto-generated method stub
		if (arg0.getNombre().compareTo(arg1.getNombre()) == 0) {
			return arg0.getDescripcion().compareTo(arg1.getDescripcion());
		} else {
			return arg0.getNombre().compareTo(arg1.getNombre());
		}
	}}
