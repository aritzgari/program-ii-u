package LN;

/**
 * Esta clase es la clase hija de articulo, sera para a�adir canciones
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsCancion extends clsArticulo {

	private String Enlace_a_youtube;
	private int idCancion;
	private int idGenero;
	private int idAlbum;
	private int idCantante;
	private int idLibreriaMultimedia;

	public clsCancion(int _idcancion, String _titulo, String _titulo_original, int _anno_de_publicacion,
			String _tipo_DoA, double _precio, boolean _en_propiedad, boolean _en_busqueda, String _formato,
			String _Enlace_a_YT, int _idGenero, int _idAlbum, int _idCantante,
			int _libreria_Multimedia_idLibreria_Multimedia) {
		// TODO Auto-generated constructor stub
		setIdCancion(_idcancion);
		setEnlace_a_youtube(_Enlace_a_YT);
		setIdGenero(_idGenero);
		setIdAlbum(_idAlbum);
		setIdCantante(_idCantante);
		setIdLibreriaMultimedia(_libreria_Multimedia_idLibreria_Multimedia);
		setTitulo(_titulo);
		setTitulo_original(_titulo_original);
		setAnno_de_publicacion(_anno_de_publicacion);
		setTipo_DoA(_tipo_DoA);
		setPrecio(_precio);
		setEn_propiedad(_en_propiedad);
		setEn_busqueda(_en_busqueda);
		setFormato(_formato);
	}

	/**
	 * @return the idCancion
	 */

	public int getIdCancion() {
		return idCancion;
	}

	/**
	 * @return the idCancion
	 */

	public void setIdCancion(int idCancion) {
		this.idCancion = idCancion;
	}

	/**
	 * @return the enlace_a_youtube
	 */
	public String getEnlace_a_youtube() {
		return Enlace_a_youtube;
	}

	/**
	 * @param enlace_a_youtube the enlace_a_youtube to set
	 */
	public void setEnlace_a_youtube(String enlace_a_youtube) {
		Enlace_a_youtube = enlace_a_youtube;
	}

	/**
	 * @return the idCantante
	 */
	public int getIdCantante() {
		return idCantante;
	}

	/**
	 * @param idCantante the idCantante to set
	 */
	public void setIdCantante(int idCantante) {
		this.idCantante = idCantante;
	}

	/**
	 * @return the idAlbum
	 */
	public int getIdAlbum() {
		return idAlbum;
	}

	/**
	 * @param idAlbum the idAlbum to set
	 */
	public void setIdAlbum(int idAlbum) {
		this.idAlbum = idAlbum;
	}

	/**
	 * @return the idGenero
	 */
	public int getIdGenero() {
		return idGenero;
	}

	/**
	 * @param idGenero the idGenero to set
	 */
	public void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	/**
	 * @return the idLibreriaMultimedia
	 */
	public int getIdLibreriaMultimedia() {
		return idLibreriaMultimedia;
	}

	/**
	 * @param idLibreriaMultimedia the idLibreriaMultimedia to set
	 */
	public void setIdLibreriaMultimedia(int idLibreriaMultimedia) {
		this.idLibreriaMultimedia = idLibreriaMultimedia;
	}

}
