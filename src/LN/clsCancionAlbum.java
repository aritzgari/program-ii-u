package LN;

import COMUN.itfProperty;
import EXCEPCIONES.clsCampoContenidoVacio;

/**
 * Esta clase servira para a�adir albumes de canciones
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 */

public class clsCancionAlbum implements itfProperty, Comparable {
	
	private int idAlbum;
	private int Cantante_idCantante = 0;
	private String Nombre = "";

	public clsCancionAlbum(int idalbum, int cantante_idCantante, String nombre) {
		// TODO Auto-generated constructor stub
		setIdAlbum(idalbum);
		Cantante_idCantante = cantante_idCantante;
		Nombre = nombre;
	}

	/**
	 * @return the cantante_idCantante
	 */
	public int getCantante_idCantante() {
		return Cantante_idCantante;
	}

	/**
	 * @param cantante_idCantante the cantante_idCantante to set
	 */
	public void setCantante_idCantante(int cantante_idCantante) {
		Cantante_idCantante = cantante_idCantante;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	/**
	 * @return the idAlbum
	 */
	public int getIdAlbum() {
		return idAlbum;
	}

	/**
	 * @param idAlbum the idAlbum to set
	 */
	public void setIdAlbum(int idAlbum) {
		this.idAlbum = idAlbum;
	}
	
	
	@Override
	public Object getProperty(String columna) {
		// TODO Auto-generated method stub
		return null;
	}

	public int compareTo(Object arg0) {

		String nom;
		clsCancionAlbum objCast;

		nom = Nombre;

		if (arg0 == null)
			throw new NullPointerException();
		if (arg0.getClass() != this.getClass())
			throw new ClassCastException();

		objCast = (clsCancionAlbum) arg0;
		// TODO Auto-generated method stub
		return nom.compareTo(objCast.getNombre());
	}

	
}
