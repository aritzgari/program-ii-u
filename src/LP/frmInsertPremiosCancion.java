package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsCancionCantante;
import LN.clsGestorLN;

/**
 * Esta clase sirve para insertar premios de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */


public class frmInsertPremiosCancion extends JFrame {

	private JPanel contentPane;

	public frmInsertPremiosCancion() {
		setTitle("Insertar Premio");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBounds(310, 533, 250, 50);
		contentPane.add(btnEnviar);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(630, 533, 250, 50);
		contentPane.add(btnSalir);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}

}
