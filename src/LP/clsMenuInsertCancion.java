package LP;

import javax.swing.JOptionPane;

import EXCEPCIONES.clsCampoContenidoVacio;
import EXCEPCIONES.clsFormatoIncorrecto;

/**
 * Esta clase es el menu de inserciones
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 * @deprecated
 */

public class clsMenuInsertCancion {
	public void clsMenuInsertCancion() {
		// TODO Auto-generated constructor stub

		int opcion = Integer.parseInt(JOptionPane.showInputDialog(
				"Esto es el menu de inserciones,�Que desea insertar?\n 1.- Insertar una cancion.\n 2.- Insertar cantante de canci�n.\n 3.- Insertar premio de una canci�n.\n 4.- Insertar genero de canci�n.\n 5.- Insertar album de canci�n.\n   Cualquier otra cosa: Salir"));
		if (opcion == 1) {

			clsInsertCancion objInsertCancion = new clsInsertCancion();

			objInsertCancion.clsInsertCancion();

		} else if (opcion == 2) {
			clsInsertCancionCantante objInsertCancionCantante = new clsInsertCancionCantante();

			objInsertCancionCantante.clsInsertCancionCantante();
		} else if (opcion == 3) {
			clsInsertCancionPremios objInsertCancionPremios = new clsInsertCancionPremios();

			objInsertCancionPremios.clsInsertCancionPremios();
		} else if (opcion == 4) {
			clsInsertCancionGenero objInsertCancionGenero = new clsInsertCancionGenero();

			objInsertCancionGenero.clsInsertCancionGenero();
		} else if (opcion == 5) {
			clsInsertCancionAlbum objInsertCancionAlbum = new clsInsertCancionAlbum();

			objInsertCancionAlbum.clsInsertCancionAlbum();

		} else {
			JOptionPane.showMessageDialog(null, "Hasta luego");
		}

	}

}
