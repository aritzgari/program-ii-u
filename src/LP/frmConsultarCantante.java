package LP;

import java.awt.BorderLayout; 
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import LN.clsCancionCantante;
import LN.clsCancionGenero;
import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Esta clase sirve para consultar cantantes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmConsultarCantante extends JFrame {

	ArrayList<clsCancionCantante> resultado = objGestorLN.consultarCantanteEnBD();
	private static clsGestorLN objGestorLN = new clsGestorLN();
	
	
	
	private JPanel contentPane;
	
	DefaultListModel modelolista = new DefaultListModel();
	private Object[][] datosfila;
	private String[] nombrescolumnas = { "ID", "NOMBRE", "APELLIDO" };
	private JTable table;

	private JScrollPane scrollPane;

	public frmConsultarCantante() {

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Consultar Cantante");
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		resultado.clear();
		resultado = objGestorLN.consultarCantanteEnBD();
		datosfila = new Object[resultado.size()][3];

		int j = 0;
		for (clsCancionCantante I : resultado) {
			datosfila[j][0] = I.getIdCantante();
			datosfila[j][1] = I.getNombre();
			datosfila[j][2] = I.getApellido();
			j++;
		}

		scrollPane = new JScrollPane();
		scrollPane.setBounds(98, 100, 1002, 390);
		contentPane.add(scrollPane);

		table = new JTable(datosfila, nombrescolumnas);
		scrollPane.setViewportView(table);
		table.setForeground(new Color(0, 0, 0));
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setBackground(Color.WHITE);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(346, 541, 500, 50);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBackground(new Color(139, 69, 19));
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);

	}
}
