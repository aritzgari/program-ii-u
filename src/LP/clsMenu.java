package LP;

import javax.swing.JOptionPane;

import EXCEPCIONES.clsCampoContenidoVacio;

/**
 * Esta clase es el menu principal
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 * @deprecated
 */

public class clsMenu {

	public void clsMenu() {
		// TODO Auto-generated constructor stub
		int opcion = 0;

		opcion = Integer.parseInt(JOptionPane.showInputDialog(
				"Esto es el menu de seleci�n, �Que quieres hacer?\n 1.- Insertar un algo\n 2.- Consultar un algo \n Cualquier otra cosa: Salir"));
		if (opcion == 1) {
			opcion = Integer.parseInt(JOptionPane.showInputDialog(
					"Esto es el menu de inserciones, �Que quieres hacer?\n 1.- Insertar algo relacionado con canciones.\n 2.- Insertar algo relacionado con peliculas. \n 3.- Insertar algo relacionado con libros. \n 4.- Insertar una libreria multimedia.\n Cualquier otra cosa: Salir"));
			if (opcion == 1) {
				clsMenuInsertCancion objMenuInsert = new clsMenuInsertCancion();
				objMenuInsert.clsMenuInsertCancion();
			} else if (opcion == 2) {
				JOptionPane.showMessageDialog(null, "Sin funci�n por el momento");
				opcion = 0;
			} else if (opcion == 3) {
				JOptionPane.showMessageDialog(null, "Sin funci�n por el momento");
				opcion = 0;
			} else if (opcion == 4) {

				clsInsertarLibreriaMultimedia objInsertLibreria = new clsInsertarLibreriaMultimedia();
				objInsertLibreria.clsInsertarLibreriaMultimedia();
				opcion = 0;
			} else {
				JOptionPane.showMessageDialog(null, "Hasta luego");
			}

		} else if (opcion == 2) {
			opcion = Integer.parseInt(JOptionPane.showInputDialog(
					"Esto es el menu de consultas, �Que quieres hacer?\n 1.- Consultar algo relacionado con canciones.\n 2.- Consultar algo relacionado con peliculas. \n 3.- Consultar algo relacionado con libros. \n 4.- Consultar una libreria multimedia. Cualquier otra cosa: Salir"));
			if (opcion == 1) {
				clsMenuConsultaCancion objMenuConsultar = new clsMenuConsultaCancion();
				objMenuConsultar.clsMenuConsultaCancion();
			} else if (opcion == 2) {
				JOptionPane.showMessageDialog(null, "Sin funci�n por el momento");
				opcion = 0;
			} else if (opcion == 3) {
				JOptionPane.showMessageDialog(null, "Sin funci�n por el momento");
				opcion = 0;
			} else if (opcion == 4) {
				clsConsultaLibreriaMultimedia objConsultarLibreria = new clsConsultaLibreriaMultimedia();
				objConsultarLibreria.clsConsultaLibreriaMultimedia();
				
			} else {
				JOptionPane.showMessageDialog(null, "Hasta luego");
			}
		} else if (opcion == 0) {

		} else {
			JOptionPane.showMessageDialog(null, "Hasta luego");
		}

	}

}
