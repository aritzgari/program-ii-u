package LP;

import java.util.ArrayList; 
import java.util.Collections;

import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;
import LN.clsOrdenacionLibreria;

public class clsConsultaLibreriaMultimedia {

	private static clsGestorLN objGestorLN = new clsGestorLN();

	public void clsConsultaLibreriaMultimedia() {

		ArrayList<clsLibreriaMultimedia> resultado = objGestorLN.consultarLibreriaMultimediaEnBD();

		System.out.println("Ordenado por id de libreria: ");
		for (clsLibreriaMultimedia L : resultado) {
			System.out.println(
					"id: " + L.getIdLibreriaMultimedia() + " Nombre: " + L.getNombre() + " Descripcion: " + L.getDescripcion());
		}

		System.out.println("Ordenado por nombre de libreria: ");

		Collections.sort(resultado, new clsOrdenacionLibreria());
		for (clsLibreriaMultimedia L : resultado) {

			System.out.println(
					"id: " + L.getIdLibreriaMultimedia() + " Nombre: " + L.getNombre() + " Descripcion: " + L.getDescripcion());
		}
	}
}
