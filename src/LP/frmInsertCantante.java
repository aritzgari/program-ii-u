package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JTextField;

public class frmInsertCantante extends JFrame {

	private JPanel contentPane;
	private JTextField txtFieldNombre;
	private JTextField textFieldApellido;

	public frmInsertCantante() {

		setTitle("Insertar Cantante");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(630, 533, 250, 50);
		contentPane.add(btnSalir);

		JButton btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (txtFieldNombre.getText().equals("") || textFieldApellido.getText().equals("")) {
					JOptionPane.showInternalMessageDialog(null, "Campos vac�os o sin editar.");
				} else {
					String nombre = txtFieldNombre.getText();
					String apellido = textFieldApellido.getText();
					clsGestorLN cantante = new clsGestorLN();
					cantante.crearcantante(nombre, apellido);
					JOptionPane.showInternalMessageDialog(null, "Insert realizado correctamente.");
				}
			}
		});
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBounds(310, 533, 250, 50);
		contentPane.add(btnEnviar);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setForeground(new Color(255, 255, 255));
		lblNombre.setBackground(new Color(0, 0, 0));
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNombre.setBounds(294, 174, 130, 70);
		contentPane.add(lblNombre);

		txtFieldNombre = new JTextField();
		txtFieldNombre.setBounds(472, 200, 500, 30);
		contentPane.add(txtFieldNombre);
		txtFieldNombre.setColumns(10);

		JLabel lblNewLabel = new JLabel("Apellido:");
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel.setBounds(294, 311, 130, 70);
		contentPane.add(lblNewLabel);

		textFieldApellido = new JTextField();
		textFieldApellido.setBounds(472, 337, 500, 30);
		contentPane.add(textFieldApellido);
		textFieldApellido.setColumns(10);

		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}
}
