package LP;

import java.awt.HeadlessException;


import javax.swing.JOptionPane;

import LN.clsGestorLN;

import EXCEPCIONES.clsCampoContenidoVacio;

/**
 * Esta clase es para la inserci�n de premios de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsInsertCancionPremios {

	public void clsInsertCancionPremios(){
		
		String nombre = JOptionPane.showInputDialog("Introduce el nombre del premio:");

		String categoria = JOptionPane.showInputDialog("Introduce la categor�a del premio:");

		int annio = Integer.parseInt(JOptionPane.showInputDialog("Introduce el a�o en el que se entreg� el premio: "));

		int idcancion = Integer.parseInt(
				JOptionPane.showInputDialog("Introduce el id de la cancion a la que se le ha dado el premio: "));

		int idgenerocancion = Integer.parseInt(JOptionPane.showInputDialog("Introduce id del genero de la canci�n: "));

		int idalbum = Integer.parseInt(JOptionPane.showInputDialog(
				"Introduce el id del album al que pertenece la cancion a la que se asigna este premio: "));

		int idcantante = Integer.parseInt(JOptionPane.showInputDialog(
				"Introduce el id del cantante que ha hecho la cancion a la que se asigna este premio: "));

		int idLibreriaMultimedia = Integer.parseInt(JOptionPane
				.showInputDialog("Introduce el id de la libreria multimedia a la que se le introduce el premio: "));
		
		
		clsGestorLN premiocancion= new clsGestorLN();

		premiocancion.crearpremiocancion(nombre, categoria,annio, idcancion, idgenerocancion, idalbum, idcantante,idLibreriaMultimedia);
	}

}
