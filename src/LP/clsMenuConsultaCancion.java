package LP;

import javax.swing.JOptionPane;

/**
 * Esta clase es el menu de consultas
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E2
 * @deprecated
 */

public class clsMenuConsultaCancion {
	public void clsMenuConsultaCancion() {
		// TODO Auto-generated constructor stub
		int opcion = Integer.parseInt(JOptionPane.showInputDialog(
				"Esto es el menu de consultas,�Que desea consultar?\n 1.- consultar las canciones.\n 2.- Consutar cantantes de canci�nes.\n 3.- Consultar premios de canci�nes.\n 4.- Consultar generos de canci�nes.\n 5.- Consultar albumes de canci�nes. \n   Cualquier otra cosa: Salir"));
		if (opcion == 1) {

			clsConsultaCancion objConsultaCancion = new clsConsultaCancion();

			objConsultaCancion.clsConsultaCancion();

		} else if (opcion == 2) {
			clsConsultaCancionCantante objConsultaCancionCantante = new clsConsultaCancionCantante();

			objConsultaCancionCantante.clsConsultaCancionCantante();
		} else if (opcion == 3) {
			clsConsultaCancionPremios objConsultaCancionPremios = new clsConsultaCancionPremios();

			objConsultaCancionPremios.clsConsultaCancionPremios();
		} else if (opcion == 4) {
			clsConsultaCancionGenero objConsultaCancionGenero = new clsConsultaCancionGenero();

			objConsultaCancionGenero.clsConsultaCancionGenero();
		} else if (opcion == 5) {
			clsConsultaCancionAlbum objConsultaCancionAlbum = new clsConsultaCancionAlbum();

			objConsultaCancionAlbum.clsConsultaCancionAlbum();
			
		} else {
			JOptionPane.showMessageDialog(null, "Hasta luego");
		}
	}
}
