package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

/**
 * Esta clase es el menu general de la aplicaci�n
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmMenu extends JFrame {

	private JPanel contentPane;

	public frmMenu() {
		// Datos de la frame
		setTitle("Menu Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(260, 130, 850, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Bot�n Insertar
		JButton btnInsertar = new JButton("INSERTAR");
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertar.setForeground(new Color(255, 255, 255));
		btnInsertar.setBackground(new Color(0, 128, 128));
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMenuInsert objmenu = new frmMenuInsert();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
			}
		});
		btnInsertar.setBounds(325, 85, 200, 50);
		contentPane.add(btnInsertar);

		// Bot�n Consultar
		JButton btnConsultar = new JButton("CONSULTAR");
		btnConsultar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultar.setForeground(new Color(255, 255, 255));
		btnConsultar.setBackground(new Color(0, 139, 139));
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmMenuConsultar objmenu = new frmMenuConsultar();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
			}
		});
		btnConsultar.setBounds(325, 185, 200, 50);
		contentPane.add(btnConsultar);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(325, 285, 200, 50);
		contentPane.add(btnSalir);

		// Label que hace las veces de fondo
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo.jpg"));
		lblFondo.setBounds(0, 0, 834, 476);
		contentPane.add(lblFondo);

	}
}
