package LP;

import java.awt.BorderLayout; 
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;

/**
 * Esta clase es el menu general de las inserciones
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmMenuInsert extends JFrame {
	private JPanel contentPane;
	private JButton btnInsertCancion;
	
	public frmMenuInsert() {
		//Datos de la frame
		setTitle("Menu Insertar");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(260, 130, 850, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnInsertCancion = new JButton("RELACIONADO CON CANCIONES");
		btnInsertCancion.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertCancion.setForeground(new Color(255, 255, 255));
		btnInsertCancion.setBackground(new Color(0, 139, 139));
		btnInsertCancion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMenuInsertCanciones objmenu = new frmMenuInsertCanciones();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
				
			}
		});
		btnInsertCancion.setBounds(110, 50, 614, 50);
		contentPane.add(btnInsertCancion);
		
		JButton btnInsertLibro = new JButton("RELACIONADO CON LIBROS");
		btnInsertLibro.setForeground(new Color(255, 255, 255));
		btnInsertLibro.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertLibro.setBackground(new Color(0, 139, 139));
		btnInsertLibro.setBounds(110, 125, 614, 50);
		contentPane.add(btnInsertLibro);
		
		JButton btnInsertPelicula = new JButton("RELACIONADO CON PELICULAS");
		btnInsertPelicula.setForeground(new Color(255, 255, 255));
		btnInsertPelicula.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertPelicula.setBackground(new Color(0, 139, 139));
		btnInsertPelicula.setBounds(110, 200, 614, 50);
		contentPane.add(btnInsertPelicula);
		
		JButton btnInsertMult = new JButton("LIBRERIAS MULTIMEDIA");
		btnInsertMult.setForeground(new Color(255, 255, 255));
		btnInsertMult.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertMult.setBackground(new Color(0, 139, 139));
		btnInsertMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmInsertLibreriaMultimedia objmenu = new frmInsertLibreriaMultimedia();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
			}
		});
		btnInsertMult.setBounds(110, 270, 614, 50);
		contentPane.add(btnInsertMult);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose(); 
			}
		});
		btnSalir.setBounds(110, 340, 614, 50);
		contentPane.add(btnSalir);
		
		
		//Label que hace las veces de fondo
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo.jpg"));
		lblFondo.setBounds(0, 0, 834, 476);
		contentPane.add(lblFondo);
		
	}
}
