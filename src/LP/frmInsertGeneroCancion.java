package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

/**
 * Esta clase sirve para insertar generos de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmInsertGeneroCancion extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;

	public frmInsertGeneroCancion() {
		setTitle("Insertar Cantante");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(461, 287, 419, 41);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		btnSalir.setBounds(630, 533, 250, 50);
		contentPane.add(btnSalir);

		JButton btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textFieldNombre.getText().equals("")) {
					JOptionPane.showInternalMessageDialog(null, "Campos vac�os o sin editar.");
				} else {
					String nombre = textFieldNombre.getText();
					clsGestorLN generocancion= new clsGestorLN();
					generocancion.creargenerocancion(nombre);
					JOptionPane.showInternalMessageDialog(null, "Insert realizado correctamente.");
				}
			}
		});
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBounds(310, 533, 250, 50);
		contentPane.add(btnEnviar);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBackground(new Color(0, 0, 0));
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNombre.setBounds(310, 274, 141, 70);
		contentPane.add(lblNombre);
		

		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}
}
