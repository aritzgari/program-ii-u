package LP;

import javax.swing.JOptionPane;  

import LN.clsGestorLN;


import EXCEPCIONES.clsCampoContenidoVacio;
import EXCEPCIONES.clsFormatoIncorrecto;

/**
 * Esta clase es para la inserción de albumes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsInsertCancionAlbum {

	public void clsInsertCancionAlbum(){

		String numero = JOptionPane.showInputDialog("Introduce el id del cantante al que pertenece el album:");

		String nombre = JOptionPane.showInputDialog("Introduce el nombre del album:");


		int id_cantante =-1;
		try {
			id_cantante = Integer.parseInt(numero);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "La id del cantante no es correcta o es nula");
		}
		
		
		if(id_cantante!=-1) {
			
			clsGestorLN album = new clsGestorLN();
			album.crearalbum(id_cantante, nombre);
		
		}else {
			
			JOptionPane.showMessageDialog(null, "No se ha ejecutado el insert");
		}

	}

}
