package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import LN.clsCancionCantante;
import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;

import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 * Esta clase sirve para insertar albumes.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmInsertAlbum extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	ArrayList<clsCancionCantante> resultado = objGestorLN.consultarCantanteEnBD();
	private static clsGestorLN objGestorLN = new clsGestorLN();
	DefaultListModel modelolista = new DefaultListModel();

	public frmInsertAlbum() {
		setTitle("Insertar Album");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(630, 533, 250, 50);
		contentPane.add(btnSalir);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(434, 328, 395, 128);
		contentPane.add(scrollPane);

		JList lista = new JList(modelolista);
		scrollPane.setViewportView(lista);
		lista.setBackground(Color.WHITE);
		lista.setForeground(Color.BLACK);
		lista.setVisibleRowCount(3);
		lista.setModel(modelolista);
		
		for (int i = 0; i < resultado.size(); i++) {
			modelolista.add(i, resultado.get(i));
		}

		JButton btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clsCancionCantante obj = (clsCancionCantante) lista.getSelectedValue();

				if (textFieldNombre.getText().equals("") || obj.getIdCantante() == 0) {
					JOptionPane.showInternalMessageDialog(null, "Campos vac�os o sin editar.");
				} else {
					String nombre = textFieldNombre.getText();
					int id_cantante = obj.getIdCantante();
					clsGestorLN album = new clsGestorLN();
					album.crearalbum(id_cantante, nombre);
					JOptionPane.showInternalMessageDialog(null, "Insert realizado correctamente.");
				}

			}
		});
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBounds(310, 533, 250, 50);
		contentPane.add(btnEnviar);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBackground(new Color(0, 0, 0));
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNombre.setBounds(294, 174, 130, 70);
		contentPane.add(lblNombre);

		JLabel lblIDCantante = new JLabel("Cantante del album:");
		lblIDCantante.setBackground(new Color(0, 0, 0));
		lblIDCantante.setForeground(Color.WHITE);
		lblIDCantante.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblIDCantante.setBounds(159, 311, 265, 70);
		contentPane.add(lblIDCantante);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(434, 191, 395, 38);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}
}
