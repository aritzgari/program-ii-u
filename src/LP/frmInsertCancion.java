package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.DefaultComboBoxModel;

/**
 * Esta clase sirve para insertar canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmInsertCancion extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldTitulo;
	private JTextField textFieldTituloOriginal;
	private JTextField textFieldFormato;
	private JTextField textFieldEnlace;

	public frmInsertCancion() {
		setTitle("Insertar Cancion");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBounds(310, 533, 250, 50);
		contentPane.add(btnEnviar);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(630, 533, 250, 50);
		contentPane.add(btnSalir);
		
		JLabel lblNewLabel = new JLabel("T\u00EDtulo:");
		lblNewLabel.setBounds(40, 60, 40, 15);
		contentPane.add(lblNewLabel);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setBounds(125, 60, 125, 20);
		contentPane.add(textFieldTitulo);
		textFieldTitulo.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("T\u00EDtulo Original:");
		lblNewLabel_1.setBounds(40, 100, 71, 15);
		contentPane.add(lblNewLabel_1);
		
		textFieldTituloOriginal = new JTextField();
		textFieldTituloOriginal.setBounds(125, 100, 125, 20);
		contentPane.add(textFieldTituloOriginal);
		textFieldTituloOriginal.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("A\u00F1o:");
		lblNewLabel_1_1.setBounds(364, 103, 71, 15);
		contentPane.add(lblNewLabel_1_1);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(0), null, null, new Integer(1)));
		spinner.setBounds(465, 103, 80, 20);
		contentPane.add(spinner);
		
		JLabel lblNewLabel_1_2 = new JLabel("Tipo digital o anal\u00F3gico:");
		lblNewLabel_1_2.setBounds(343, 65, 112, 15);
		contentPane.add(lblNewLabel_1_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Digital", "Anal\u00F3gico"}));
		comboBox.setBounds(465, 61, 80, 22);
		contentPane.add(comboBox);
		
		JLabel lblNewLabel_1_3 = new JLabel("Formato:");
		lblNewLabel_1_3.setBounds(40, 144, 71, 15);
		contentPane.add(lblNewLabel_1_3);
		
		textFieldFormato = new JTextField();
		textFieldFormato.setColumns(10);
		textFieldFormato.setBounds(125, 140, 125, 20);
		contentPane.add(textFieldFormato);
		
		JLabel lblNewLabel_1_3_1 = new JLabel("Enlace a youtube:");
		lblNewLabel_1_3_1.setBounds(20, 183, 98, 15);
		contentPane.add(lblNewLabel_1_3_1);
		
		textFieldEnlace = new JTextField();
		textFieldEnlace.setColumns(10);
		textFieldEnlace.setBounds(125, 180, 125, 20);
		contentPane.add(textFieldEnlace);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}
}
