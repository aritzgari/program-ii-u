package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Esta clase es el menu de las cosultas sobre canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmMenuConsultarCanciones extends JFrame {

	private JPanel contentPane;

	public frmMenuConsultarCanciones() {

		setTitle("Menu Consultar Canciones");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JEditorPane descripcion = new JEditorPane();
		descripcion.setForeground(Color.WHITE);
		descripcion.setEditable(false);
		descripcion.setFont(new Font("Calibri", Font.BOLD, 20));
		descripcion.setBackground(Color.BLACK);
		descripcion.setText(
				"Acabas de acceder al apartado de consulta de art\u00EDculos relacionados con canciones.  Tiene diversas opciones, puedes consultar informaci\u00F3n sobre una canci\u00F3n, sobre un   cantante, sobre un alb\u00FAm, sobre un g\u00E9nero de cancion o sobre alg\u00FAn premio que se   le haya otorgado.");
		descripcion.setBounds(230, 44, 724, 110);
		contentPane.add(descripcion);

		JButton btnConsultarCantante = new JButton("CONSULTAR CANTANTE");
		btnConsultarCantante.setForeground(new Color(255, 255, 255));
		btnConsultarCantante.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarCantante.setBackground(new Color(32, 178, 170));
		btnConsultarCantante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmConsultarCantante objcantante = new frmConsultarCantante();
				objcantante.setVisible(true);
				objcantante.setResizable(false);
				
			}
		});
		btnConsultarCantante.setBounds(50, 230, 500, 50);
		contentPane.add(btnConsultarCantante);

		JButton btnConsultarGenero = new JButton("CONSULTAR GENERO");
		btnConsultarGenero.setForeground(new Color(255, 255, 255));
		btnConsultarGenero.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarGenero.setBackground(new Color(32, 178, 170));
		btnConsultarGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmConsultaGeneroCancion objgenero = new frmConsultaGeneroCancion();
				objgenero.setVisible(true);
				objgenero.setResizable(false);
			}
		});
		btnConsultarGenero.setBounds(50, 358, 500, 50);
		contentPane.add(btnConsultarGenero);

		JButton btnConsultarAlbum = new JButton("CONSULTAR ALBUM");
		btnConsultarAlbum.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarAlbum.setForeground(new Color(255, 255, 255));
		btnConsultarAlbum.setBackground(new Color(32, 178, 170));
		btnConsultarAlbum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmConsultarAlbum objalbum = new frmConsultarAlbum();
				objalbum.setVisible(true);
				objalbum.setResizable(false);
			}
		});
		btnConsultarAlbum.setBounds(632, 230, 500, 50);
		contentPane.add(btnConsultarAlbum);

		JButton btnConsultarPremios = new JButton("CONSULTAR PREMIOS");
		btnConsultarPremios.setForeground(new Color(255, 255, 255));
		btnConsultarPremios.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarPremios.setBackground(new Color(32, 178, 170));
		btnConsultarPremios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmConsultarPremiosCancion objPremios = new frmConsultarPremiosCancion();
				objPremios.setVisible(true);
				objPremios.setResizable(false);
			}
		});
		btnConsultarPremios.setBounds(634, 358, 500, 50);
		contentPane.add(btnConsultarPremios);

		JButton btnConsultarCanciones = new JButton("CONSULTAR CANCIONES");
		btnConsultarCanciones.setForeground(new Color(255, 255, 255));
		btnConsultarCanciones.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarCanciones.setBackground(new Color(32, 178, 170));
		btnConsultarCanciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmConsultarCancion objcancion = new frmConsultarCancion();
				objcancion.setVisible(true);
				objcancion.setResizable(false);
			}
		});
		btnConsultarCanciones.setBounds(346, 449, 500, 50);
		contentPane.add(btnConsultarCanciones);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(346, 541, 500, 50);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBackground(new Color(139, 69, 19));
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);

	}
}
