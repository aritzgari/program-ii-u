package LP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import COMUN.itfProperty;
import LN.clsCancionAlbum;
import LN.clsGestorLN;
import LN.clsOrdenacionAlbum;

/**
 * Esta clase es para consultar albumes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsConsultaCancionAlbum {

	private static clsGestorLN objGestorLN = new clsGestorLN();

	public void clsConsultaCancionAlbum() {
		ArrayList<clsCancionAlbum> resultado = objGestorLN.consultarAlbumEnBD();

		System.out.println("Ordenado por id de album: ");
		for (clsCancionAlbum L : resultado) {
			System.out.println("id: " + L.getIdAlbum() + " Nombre: " + L.getNombre()
					+ " Id del cantante al que le pertenece: " + L.getCantante_idCantante());
		}

		System.out.println("Ordenado por nombre de album: ");

		Collections.sort(resultado, new clsOrdenacionAlbum());
		for (clsCancionAlbum L : resultado) {

			System.out.println("id: " + L.getIdAlbum() + " Nombre: " + L.getNombre()
					+ " Id del cantante al que le pertenece: " + L.getCantante_idCantante());
		}

	}

}
