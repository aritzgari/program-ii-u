package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import LN.clsCancion;
import LN.clsCancionAlbum;
import LN.clsCancionCantante;
import LN.clsCancionGenero;
import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;

/**
 * Esta clase sirve para consultar canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmConsultarCancion extends JFrame {

	private static clsGestorLN objGestorLN = new clsGestorLN();
	ArrayList<clsCancion> resultado = objGestorLN.consultarCancionEnBD();

	private static clsGestorLN objGestorLN2 = new clsGestorLN();
	ArrayList<clsCancionGenero> resultado2 = objGestorLN2.consultarCancionGeneroEnBD();

	private static clsGestorLN objGestorLN3 = new clsGestorLN();
	ArrayList<clsCancionAlbum> resultado3 = objGestorLN3.consultarAlbumEnBD();

	private static clsGestorLN objGestorLN5 = new clsGestorLN();
	ArrayList<clsCancionCantante> resultado5 = objGestorLN5.consultarCantanteEnBD();

	private static clsGestorLN objGestorLN6 = new clsGestorLN();
	ArrayList<clsLibreriaMultimedia> resultado6 = objGestorLN6.consultarLibreriaMultimediaEnBD();

	DefaultListModel modelolista = new DefaultListModel();
	private Object[][] datosfila;
	private String[] nombrescolumnas = { "ID", "TITULO", "TITULO ORIGINAL", "A�O", "TIPO", "PRECIO", "EN POSESION",
			"EN BUSQUEDA", "FORMATO", "ENLACE", "GENERO", "ALBUM", "CANTANTE", "LIBRERIA" };
	private JTable table;

	private JPanel contentPane;

	private JScrollPane scrollPane;

	public frmConsultarCancion() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Consultar Canci�n");
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		resultado.clear();
		resultado = objGestorLN.consultarCancionEnBD();
		datosfila = new Object[resultado.size()][14];

		String aux = "";

		int j = 0;
		for (clsCancion I : resultado) {
			datosfila[j][0] = I.getIdCancion();
			datosfila[j][1] = I.getTitulo();
			datosfila[j][2] = I.getTitulo_original();
			datosfila[j][3] = I.getAnno_de_publicacion();
			datosfila[j][4] = I.getTipo_DoA();
			datosfila[j][5] = I.getPrecio();
			if (I.isEn_propiedad() == true) {
				datosfila[j][6] = "Si";
			} else {
				datosfila[j][6] = "No";
			}
			if (I.isEn_busqueda() == true) {
				datosfila[j][7] = "Si";
			} else {
				datosfila[j][7] = "No";
			}
			datosfila[j][8] = I.getFormato();
			datosfila[j][9] = I.getEnlace_a_youtube();
			for (clsCancionGenero a : resultado2) {
				if (a.getIdGenero() == I.getIdGenero()) {
					aux = a.getNombre();
				}
			}
			datosfila[j][10] = aux;
			for (clsCancionAlbum a : resultado3) {
				if (a.getIdAlbum() == I.getIdAlbum()) {
					aux = a.getNombre();
				}
			}
			datosfila[j][11] = aux;
			for (clsCancionCantante a : resultado5) {
				if (a.getIdCantante() == I.getIdCantante()) {
					aux = a.getNombre();
				}
			}
			datosfila[j][12] = aux;

			for (clsLibreriaMultimedia a : resultado6) {
				if (a.getIdLibreriaMultimedia() == I.getIdLibreriaMultimedia()) {
					aux = a.getNombre();
				}
			}

			datosfila[j][13] = aux;
			j++;
		}

		scrollPane = new JScrollPane();
		scrollPane.setBounds(98, 100, 1002, 390);
		contentPane.add(scrollPane);

		table = new JTable(datosfila, nombrescolumnas);
		scrollPane.setViewportView(table);
		table.setForeground(new Color(0, 0, 0));
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setBackground(Color.WHITE);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(346, 541, 500, 50);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBackground(new Color(139, 69, 19));
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}

}
