package LP;

import javax.swing.JOptionPane;

import LN.clsGestorLN;

/**
 * Esta clase es para la inserción de cantantes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 * @deprecated
 */

public class clsInsertCancionCantante {

	public void clsInsertCancionCantante() {

		String nombre = JOptionPane.showInputDialog("Introduce el nombre del cantante:");

		String apellido = JOptionPane.showInputDialog("Introduce el apellido del cantante:");

		clsGestorLN cantante = new clsGestorLN();

		cantante.crearcantante(nombre, apellido);

	}

}
