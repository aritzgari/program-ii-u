package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.DropMode;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JEditorPane;
import java.awt.Button;
import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Esta clase es el menu de selecci�n de inserciones relacionadas con canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmMenuInsertCanciones extends JFrame {

	private JPanel contentPane;

	public frmMenuInsertCanciones() {

		setTitle("Menu Insertar Canciones");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JEditorPane descripcion = new JEditorPane();
		descripcion.setForeground(Color.WHITE);
		descripcion.setEditable(false);
		descripcion.setFont(new Font("Calibri", Font.BOLD, 20));
		descripcion.setBackground(Color.BLACK);
		descripcion.setText(
				"Acabas de acceder al apartado de inserci\u00F3n de art\u00EDculos relacionados con canciones.  Tiene diversas opciones, puedes insertar informaci\u00F3n sobre una canci\u00F3n, sobre un     cantante, sobre un alb\u00FAm, sobre un g\u00E9nero de cancion o sobre alg\u00FAn premio que se   le haya otorgado.");
		descripcion.setBounds(230, 44, 724, 110);
		contentPane.add(descripcion);

		JButton btnInsertarCantante = new JButton("INSERTAR CANTANTE");
		btnInsertarCantante.setForeground(new Color(255, 255, 255));
		btnInsertarCantante.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertarCantante.setBackground(new Color(32, 178, 170));
		btnInsertarCantante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmInsertCantante objcantante = new frmInsertCantante();
				objcantante.setVisible(true);
				objcantante.setResizable(false);
			}
		});
		btnInsertarCantante.setBounds(50, 230, 500, 50);
		contentPane.add(btnInsertarCantante);

		JButton btnInsertarGenero = new JButton("INSERTAR GENERO");
		btnInsertarGenero.setForeground(new Color(255, 255, 255));
		btnInsertarGenero.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertarGenero.setBackground(new Color(32, 178, 170));
		btnInsertarGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmInsertGeneroCancion objgenero = new frmInsertGeneroCancion();
				objgenero.setVisible(true);
				objgenero.setResizable(false);
			}
		});
		btnInsertarGenero.setBounds(50, 358, 500, 50);
		contentPane.add(btnInsertarGenero);

		JButton btnInsertarAlbum = new JButton("INSERTAR ALBUM");
		btnInsertarAlbum.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertarAlbum.setForeground(new Color(255, 255, 255));
		btnInsertarAlbum.setBackground(new Color(32, 178, 170));
		btnInsertarAlbum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmInsertAlbum objalbum = new frmInsertAlbum();
				objalbum.setVisible(true);
				objalbum.setResizable(false);
			}
		});
		btnInsertarAlbum.setBounds(632, 230, 500, 50);
		contentPane.add(btnInsertarAlbum);

		JButton btnInsertarPremios = new JButton("INSERTAR PREMIOS");
		btnInsertarPremios.setForeground(new Color(255, 255, 255));
		btnInsertarPremios.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertarPremios.setBackground(new Color(32, 178, 170));
		btnInsertarPremios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmInsertPremiosCancion objpremio = new frmInsertPremiosCancion();
				objpremio.setVisible(true);
				objpremio.setResizable(false);
			}
		});
		btnInsertarPremios.setBounds(634, 358, 500, 50);
		contentPane.add(btnInsertarPremios);

		JButton btnInsertarCanciones = new JButton("INSERTAR CANCIONES");
		btnInsertarCanciones.setForeground(new Color(255, 255, 255));
		btnInsertarCanciones.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnInsertarCanciones.setBackground(new Color(32, 178, 170));
		btnInsertarCanciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmInsertCancion objcancion = new frmInsertCancion();
				objcancion.setVisible(true);
				objcancion.setResizable(false);
			}
		});
		btnInsertarCanciones.setBounds(346, 449, 500, 50);
		contentPane.add(btnInsertarCanciones);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(346, 541, 500, 50);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBackground(new Color(139, 69, 19));
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}
}
