package LP;

import javax.swing.JOptionPane;

import LN.clsGestorLN;

/**
 * Esta clase es para la inserción de generos de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsInsertCancionGenero {

	public void clsInsertCancionGenero() {

		String nombre = JOptionPane.showInputDialog("Introduce el nombre del genero:");
		
		clsGestorLN generocancion= new clsGestorLN();

		generocancion.creargenerocancion(nombre);


	}

}
