package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;

/**
 * Esta clase es el menu general de las consultas
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmMenuConsultar extends JFrame {
	private JPanel contentPane;

	public frmMenuConsultar() {
		// Datos de la frame
		setTitle("Menu Consultar");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(260, 130, 850, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnConsultarLibro = new JButton("RELACIONADO CON LIBROS");
		btnConsultarLibro.setForeground(new Color(255, 255, 255));
		btnConsultarLibro.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarLibro.setBackground(new Color(0, 139, 139));
		btnConsultarLibro.setBounds(110, 125, 614, 50);
		contentPane.add(btnConsultarLibro);

		JButton btnConsultarPelicula = new JButton("RELACIONADO CON PELICULAS");
		btnConsultarPelicula.setForeground(new Color(255, 255, 255));
		btnConsultarPelicula.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarPelicula.setBackground(new Color(0, 139, 139));
		btnConsultarPelicula.setBounds(110, 200, 614, 50);
		contentPane.add(btnConsultarPelicula);

		JButton btnConsultarMult = new JButton("LIBRERIAS MULTIMEDIA");
		btnConsultarMult.setForeground(new Color(255, 255, 255));
		btnConsultarMult.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarMult.setBackground(new Color(0, 139, 139));
		btnConsultarMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmConsultarLibreriaMultimedia objmenu = new frmConsultarLibreriaMultimedia();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
			}
		});
		btnConsultarMult.setBounds(110, 275, 614, 50);
		contentPane.add(btnConsultarMult);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(110, 350, 614, 50);
		contentPane.add(btnSalir);

		JButton btnConsultarCancion = new JButton("RELACIONADO CON CANCIONES");
		btnConsultarCancion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMenuConsultarCanciones objmenu = new frmMenuConsultarCanciones();
				objmenu.setVisible(true);
				objmenu.setResizable(false);
				
			}
		});
		btnConsultarCancion.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnConsultarCancion.setBackground(new Color(0, 139, 139));
		btnConsultarCancion.setForeground(new Color(255, 255, 255));
		btnConsultarCancion.setBounds(110, 50, 614, 50);
		contentPane.add(btnConsultarCancion);

		// Label que hace las veces de fondo
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo.jpg"));
		lblFondo.setBounds(0, 0, 834, 476);
		contentPane.add(lblFondo);

	}

}
