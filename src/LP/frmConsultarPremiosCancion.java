package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import LN.clsCancion;
import LN.clsCancionAlbum;
import LN.clsCancionCantante;
import LN.clsCancionGenero;
import LN.clsCancionPremios;
import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;

/**
 * Esta clase sirve para consultar cantantes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmConsultarPremiosCancion extends JFrame {

	private static clsGestorLN objGestorLN = new clsGestorLN();
	ArrayList<clsCancionPremios> resultado = objGestorLN.consultarPremiosCancionEnBD();

	private static clsGestorLN objGestorLN2 = new clsGestorLN();
	ArrayList<clsCancion> resultado2 = objGestorLN2.consultarCancionEnBD();
	
	private static clsGestorLN objGestorLN3 = new clsGestorLN();
	ArrayList<clsCancionGenero> resultado3 = objGestorLN3.consultarCancionGeneroEnBD();
	
	private static clsGestorLN objGestorLN4 = new clsGestorLN();
	ArrayList<clsCancionAlbum> resultado4 = objGestorLN4.consultarAlbumEnBD();
	
	private static clsGestorLN objGestorLN5 = new clsGestorLN();
	ArrayList<clsCancionCantante> resultado5 = objGestorLN5.consultarCantanteEnBD();
	
	private static clsGestorLN objGestorLN6 = new clsGestorLN();
	ArrayList<clsLibreriaMultimedia> resultado6 = objGestorLN6.consultarLibreriaMultimediaEnBD();


	private JPanel contentPane;

	DefaultListModel modelolista = new DefaultListModel();
	private Object[][] datosfila;
	private String[] nombrescolumnas = { "ID", "NOMBRE", "CATEGORIA", "A�O", "CANCION", "GENERO", "ALBUM", "CANTANTE",
			"LIBRERIA" };
	private JTable table;

	private JScrollPane scrollPane;

	public frmConsultarPremiosCancion() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Consultar Premios");
		setBounds(100, 20, 1200, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		resultado.clear();
		resultado = objGestorLN.consultarPremiosCancionEnBD();
		datosfila = new Object[resultado.size()][9];
		String cancion = "";
		String genero ="";
		String album ="";
		String cantante ="";
		String libreria ="";

		int j = 0;
		for (clsCancionPremios I : resultado) {
			datosfila[j][0] = I.getIdPremios();
			datosfila[j][1] = I.getNombre();
			datosfila[j][2] = I.getCategoria();
			datosfila[j][3] = I.getA�o();
			for (clsCancion a : resultado2) {
				if (a.getIdCancion()== I.getCanciones_idCanciones()) {
					cancion = a.getTitulo();
					System.out.println(cancion);
				}
			}
			datosfila[j][4] = I.getCanciones_idCanciones();
			for (clsCancionGenero a : resultado3) {
				if (a.getIdGenero() == I.getCanciones_G�nero_Canci�n_idG�nero_Canci�n()) {
					genero = a.getNombre();
				}
			}
			datosfila[j][5] = genero;
			for (clsCancionAlbum a : resultado4) {
				if (a.getIdAlbum() == I.getCanciones_Album_idAlbum()) {
					album = a.getNombre();
				}
			}
			datosfila[j][6] = album;
			for (clsCancionCantante a : resultado5) {
				if (a.getIdCantante() == I.getCanciones_Album_Cantante_idCantante()) {
					cantante = a.getNombre();
				}
			}
			datosfila[j][7] = cantante;
			
			for (clsLibreriaMultimedia a : resultado6) {
				if (a.getIdLibreriaMultimedia() == I.getCanciones_Libreria_Multimedia_idLibreria_Multimedia()) {
					libreria = a.getNombre();
				}
			}
			datosfila[j][8] = libreria;
			j++;
		}

		scrollPane = new JScrollPane();
		scrollPane.setBounds(98, 100, 1002, 390);
		contentPane.add(scrollPane);

		table = new JTable(datosfila, nombrescolumnas);
		scrollPane.setViewportView(table);
		table.setForeground(new Color(0, 0, 0));
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setBackground(Color.WHITE);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(220, 20, 60));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(346, 541, 500, 50);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setBackground(new Color(139, 69, 19));
		lblFondo.setIcon(new ImageIcon("img/fondo2.jpg"));
		lblFondo.setBounds(0, 0, 1184, 661);
		contentPane.add(lblFondo);
	}

}
