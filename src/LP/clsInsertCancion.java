package LP;

import javax.swing.JOptionPane;

import LN.clsGestorLN;

/**
 * Esta clase es para la inserci�n de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsInsertCancion {

	public void clsInsertCancion() {

		String titulo = JOptionPane.showInputDialog("Introduce el titulo de la cancion: ");

		String titulo_original = JOptionPane.showInputDialog("Introduce el titulo original de la canci�n: ");

		int anno_de_publicacion = Integer.parseInt(JOptionPane.showInputDialog("Introduce el a�o en el que ha salido la canci�n: "));
		String tipo_DoA = JOptionPane
				.showInputDialog("Introduce el formato en el que posees la canci�n, si digital o anal�gico: ");

		Double precio = Double.parseDouble(JOptionPane.showInputDialog("Introduce el precio de la canci�n: "));

		boolean en_propiedad = Boolean.parseBoolean(JOptionPane.showInputDialog("�Lo tienes? 1 si, 0 no. "));

		boolean en_busqueda = Boolean.parseBoolean(JOptionPane.showInputDialog("�Lo quieres? 1 si, 0 no. "));

		String formato = JOptionPane.showInputDialog("Introduce el tipo de archivo (mp3,mp4,mp5..): ");

		String Enlace_a_YT = JOptionPane.showInputDialog("Introduce el enlace a You Tube: ");

		int idGenero = Integer.parseInt(JOptionPane.showInputDialog("Introduce id del genero de la canci�n: "));

		int idAlbum = Integer.parseInt(JOptionPane.showInputDialog(
				"Introduce el id del album al que pertenece la cancion a la que se asigna este premio: "));

		int idCantante = Integer.parseInt(JOptionPane.showInputDialog(
				"Introduce el id del cantante que ha hecho la cancion a la que se asigna este premio: "));

		int Libreria_Multimedia_idLibreria_Multimedia = Integer.parseInt(JOptionPane
				.showInputDialog("Introduce el id de la libreria multimedia a la que se le introduce el premio: "));
		
		clsGestorLN cancion = new clsGestorLN();
		
		cancion.crearcancion(Libreria_Multimedia_idLibreria_Multimedia, titulo, titulo_original, anno_de_publicacion, idGenero, idAlbum, idCantante, tipo_DoA, precio, en_propiedad, en_busqueda, formato, Enlace_a_YT);


	}

}
