package LP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;

import LN.clsGestorLN;
import LN.clsLibreriaMultimedia;
import LN.clsOrdenacionLibreria;

import javax.swing.JList;
import javax.swing.JToggleButton;
import javax.swing.JScrollPane;

/**
 * Esta clase sirve para consultar librerias multimedia.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */
public class frmConsultarLibreriaMultimedia extends JFrame {

	private JPanel contentPane;
	private static clsGestorLN objGestorLN = new clsGestorLN();
	DefaultListModel modelolista = new DefaultListModel();
	ArrayList<clsLibreriaMultimedia> resultado = objGestorLN.consultarLibreriaMultimediaEnBD();

	public frmConsultarLibreriaMultimedia() {

		setTitle("Consultar Librerias Multimedia");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(285, 138, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(125, 76, 545, 300);
		contentPane.add(scrollPane);

		JList lista_1 = new JList(modelolista);
		scrollPane.setViewportView(lista_1);
		lista_1.setBackground(Color.LIGHT_GRAY);
		lista_1.setForeground(Color.BLACK);
		lista_1.setVisibleRowCount(3);
		lista_1.setModel(modelolista);

		for (int i = 0; i < resultado.size(); i++) {
			modelolista.add(i, resultado.get(i));
		}

		JButton btnOrdenacion = new JButton("Ordenaci\u00F3n por nombre");
		btnOrdenacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Collections.sort(resultado, new clsOrdenacionLibreria());
				modelolista.clear();
				JList lista = new JList(modelolista);
				lista.setBackground(Color.LIGHT_GRAY);
				lista.setForeground(Color.BLACK);
				lista.setVisibleRowCount(3);
				lista.setBounds(125, 76, 545, 300);
				lista.setModel(modelolista);
				contentPane.add(lista);
				for (int i = 0; i < resultado.size(); i++) {
					modelolista.add(i, resultado.get(i));
				}

			}
		});
		btnOrdenacion.setBounds(125, 29, 200, 25);
		contentPane.add(btnOrdenacion);

		JButton btnOrdenacion2 = new JButton("Ordenacion por id");
		btnOrdenacion2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resultado.clear();
				resultado = objGestorLN.consultarLibreriaMultimediaEnBD();
				modelolista.clear();
				JList lista = new JList(modelolista);
				lista.setBackground(Color.LIGHT_GRAY);
				lista.setForeground(Color.BLACK);
				lista.setVisibleRowCount(3);
				lista.setBounds(125, 76, 545, 300);
				lista.setModel(modelolista);
				contentPane.add(lista);
				for (int i = 0; i < resultado.size(); i++) {
					modelolista.add(i, resultado.get(i));
				}
			}

		});
		btnOrdenacion2.setBounds(470, 29, 200, 25);
		contentPane.add(btnOrdenacion2);

		JButton btnSalir = new JButton("SALIR");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setBackground(Color.RED);
		btnSalir.setBounds(300, 400, 200, 25);
		btnSalir.setForeground(Color.WHITE);
		contentPane.add(btnSalir);

		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo3.jpg"));
		lblFondo.setBounds(0, 0, 800, 500);
		contentPane.add(lblFondo);

	}
}
