package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Esta clase sirve para insertar librerias multimedia.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E5
 */

public class frmInsertLibreriaMultimedia extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldDescripci�n;
	private JButton btnSalir;
	private JButton btnEnviar;

	public frmInsertLibreriaMultimedia() {

		setTitle("Insertar Librerias Multimedia");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(285, 138, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBackground(Color.WHITE);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNombre.setForeground(new Color(255, 255, 255));
		lblNombre.setBounds(110, 72, 117, 60);
		contentPane.add(lblNombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setBackground(Color.WHITE);
		textFieldNombre.setBounds(293, 90, 365, 30);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblDescripcion = new JLabel("Descripci\u00F3n:");
		lblDescripcion.setBackground(new Color(255, 255, 255));
		lblDescripcion.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblDescripcion.setForeground(new Color(255, 255, 255));
		lblDescripcion.setBounds(110, 162, 162, 50);
		contentPane.add(lblDescripcion);

		textFieldDescripci�n = new JTextField();
		textFieldDescripci�n.setBounds(293, 175, 365, 30);
		contentPane.add(textFieldDescripci�n);
		textFieldDescripci�n.setColumns(10);

		btnSalir = new JButton("SALIR");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setForeground(new Color(255, 255, 255));
		btnSalir.setBackground(new Color(255, 0, 0));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSalir.setBounds(407, 295, 251, 60);
		contentPane.add(btnSalir);

		btnEnviar = new JButton("ENVIAR");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (textFieldNombre.getText().equals("") || textFieldDescripci�n.getText().equals("")) {
					JOptionPane.showInternalMessageDialog(null,"Campos vac�os o sin editar.");
				} else {
					String nombre = textFieldNombre.getText();
					String descripcion = textFieldDescripci�n.getText();
					clsGestorLN objLibreriaMultimedia = new clsGestorLN();
					objLibreriaMultimedia.crearaLibreriaMultimedia(nombre, descripcion);
					JOptionPane.showInternalMessageDialog(null,"Insert realizado correctamente.");
				}
			}
		});
		btnEnviar.setForeground(new Color(255, 255, 255));
		btnEnviar.setBackground(new Color(0, 255, 0));
		btnEnviar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEnviar.setBounds(109, 293, 251, 60);
		contentPane.add(btnEnviar);

		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon("img/fondo3.jpg"));
		lblFondo.setBounds(0, 0, 800, 500);
		contentPane.add(lblFondo);
	}
}
