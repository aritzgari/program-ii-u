package LP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import COMUN.itfProperty;
import LN.clsCancionCantante;
import LN.clsGestorLN;
import LN.clsOrdenacionCantante;
import COMUN.Constantes;

/**
 * Esta clase es para consultar cantantes de canciones.
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E3
 */

public class clsConsultaCancionCantante {
	private static clsGestorLN objGestorLN = new clsGestorLN();

	public void clsConsultaCancionCantante() {

		ArrayList<clsCancionCantante> resultado = objGestorLN.consultarCantanteEnBD();

		System.out.println("Ordenado por id de cantante: ");
		for (clsCancionCantante L : resultado) {
			System.out.println(
					"id: " + L.getIdCantante() + " Nombre: " + L.getNombre() + " Apellido: " + L.getApellido());
		}

		System.out.println("Ordenado por nombre de cantante: ");

		Collections.sort(resultado, new clsOrdenacionCantante());
		for (clsCancionCantante L : resultado) {

			System.out.println(
					"id: " + L.getIdCantante() + " Nombre: " + L.getNombre() + " Apellido: " + L.getApellido());
		}
	}

}
