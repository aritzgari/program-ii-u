package EXCEPCIONES;

/**
 * Esta clase es para la excepci�n de una variable mal escrita
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E4
 */

public class clsFormatoIncorrecto extends Exception {

	public final String mensaje = "Alguna variable tiene un formato incorrecto";

	@Override
	public String getMessage() {

		return mensaje;
	}

}
