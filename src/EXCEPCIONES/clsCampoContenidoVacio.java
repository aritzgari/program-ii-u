package EXCEPCIONES;

/**
 * Esta clase es para la excepci�n de contenido vacio en un campo
 * 
 * @author Aritz Garitano y Iker Reyes
 * @since E4
 */

public class clsCampoContenidoVacio extends Exception {

	public final String mensaje = "Alg�n atributo vacio.";

	@Override
	public String getMessage() {

		return mensaje;
	}

}
